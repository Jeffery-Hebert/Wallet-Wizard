steps = [
    [
        """
        CREATE TABLE assets (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(255) NOT NULL,
            type VARCHAR(15) NOT NULL CHECK (type IN ('liquid', 'semi-liquid', 'illiquid')),
            value INTEGER NOT NULL,
            userID VARCHAR(255) NOT NULL
        );

        """,
        """
        DROP TABLE assets;
        """,
    ],
    [
        """
        CREATE TABLE liabilities (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(255) NOT NULL,
            type VARCHAR(17) NOT NULL CHECK (type IN ('short-term', 'intermediate-term', 'long-term')),
            value INTEGER NOT NULL,
            userID VARCHAR(255) NOT NULL
        );

        """,
        # drop the table
        """
        DROP TABLE liabilities;
        """,
    ],
    [
        """
        CREATE TABLE net_worth (
            id SERIAL PRIMARY KEY NOT NULL,
            value INTEGER NOT NULL,
            date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            userID VARCHAR(255) NOT NULL
        );

        """,
        """
        DROP TABLE net_worth;
        """,
    ],
    [
        """
        CREATE TABLE expenses (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(255) NOT NULL,
            amount INTEGER NOT NULL,
            type VARCHAR(20) NOT NULL CHECK (type IN ('Car', 'Housing', 'Utilities','Food','Phone/TV','Entertainment/Fun','Other')),
            userID VARCHAR(255) NOT NULL
        );

        """,
        """
        DROP TABLE expenses;
        """,
    ],
    [
        """
        CREATE TABLE income (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(255) NOT NULL,
            annualized_pay INTEGER NOT NULL,
            pay_frequency VARCHAR(15) NOT NULL CHECK (pay_frequency IN ('Weekly', 'Bi-Weekly', 'Twice a month','Monthly','Other')),
            userID VARCHAR(255) NOT NULL
        );

        """,
        """
        DROP TABLE income;
        """,
    ],
]
