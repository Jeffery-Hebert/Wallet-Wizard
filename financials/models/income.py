from pydantic import BaseModel


class IncomeIn(BaseModel):
    name: str
    annualized_pay: int
    pay_frequency: str


class IncomeOut(BaseModel):
    id: str
    name: str
    annualized_pay: int
    pay_frequency: str


class IncomeList(BaseModel):
    income_list: list[IncomeOut]
