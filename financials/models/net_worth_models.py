from pydantic import BaseModel
from datetime import datetime


class NetWorthIn(BaseModel):
    value: int
    date: datetime


class NetWorthOut(BaseModel):
    id: str
    value: int
    date: datetime


class NetWorthList(BaseModel):
    net_worth_list: list[NetWorthOut]
