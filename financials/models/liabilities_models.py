from pydantic import BaseModel


class LiabilityIn(BaseModel):
    name: str
    value: int
    type: str


class LiabilityOut(BaseModel):
    id: str
    name: str
    value: int
    type: str


class LiabilitiesList(BaseModel):
    liabilities: list[LiabilityOut]
