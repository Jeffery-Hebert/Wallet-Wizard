from models.assets_models import AssetOut, AssetsList
from queries.pool import pool


class Assets:
    def create(self, asset, userID):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO assets
                    (
                        name,
                        type,
                        value,
                        userID
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [asset.name, asset.type, asset.value, userID],
                )
                id = result.fetchone()[0]
                data = asset.dict()
                return AssetOut(id=id, **data)

    def getlist(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM assets
                        WHERE userID = %s
                        ORDER BY value
                        """,
                        [userID],
                    )
                    a = [
                        AssetOut(
                            id=record[0],
                            name=record[1],
                            value=record[3],
                            type=record[2],
                        )
                        for record in result
                    ]
                    response = AssetsList(assets=a)
                    return response
        except Exception as e:
            print(e)
            return {"message": "Could not get all assets"}

    def getindv(self, id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        name,
                        type,
                        value
                        FROM assets
                        WHERE id = %s AND userID = %s
                        """,
                        [id, userID],
                    )
                    record = result.fetchone()
                    if record is None:
                        return {"message": "The asset id is not asset"}
                    return self.out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get asset"}

    def delete(self, asset_id, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM assets
                        WHERE id = %s AND userID = %s
                        """,
                        [asset_id, userID],
                    )
                    return True
        except Exception:
            return False

    def edit(self, id, asset, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE assets
                        SET name = %s, type = %s, value = %s
                        WHERE id = %s AND userID = %s
                        """,
                        [asset.name, asset.type, asset.value, id, userID],
                    )
                    data = asset.dict()
                    return AssetOut(id=id, **data)
        except Exception as e:
            print(e)
            return {"message": "Could not update asset"}

    def out(self, record):
        return AssetOut(
            id=record[0], name=record[1], value=record[3], type=record[2]
        )

    def total(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT SUM(value) as total_assets
                        FROM assets
                        WHERE userID = %s
                        """,
                        [userID],
                    )
                    result = result.fetchone()[0]
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get the total asset value"}
