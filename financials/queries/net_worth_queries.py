from models.net_worth_models import NetWorthOut, NetWorthList
from queries.pool import pool
from queries.asset_queries import Assets
from queries.liability_queries import Liabilities


class NetWorth:
    def create(self, userID):
        asset_total = Assets.total(self, userID)
        liability_total = Liabilities.total(self, userID)
        if asset_total is None:
            asset_total = 0
        if liability_total is None:
            liability_total = 0
        net_worth = asset_total - liability_total
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO net_worth
                    (
                        value,
                        userID
                    )
                    VALUES (%s, %s)
                    RETURNING id;
                    """,
                    [net_worth, userID],
                )
                return True

    def getAll(self, userID):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM net_worth
                        WHERE userID = %s
                        ORDER BY date desc
                        """,
                        [userID],
                    )
                    a = [
                        NetWorthOut(
                            id=record[0], value=record[1], date=record[2]
                        )
                        for record in result
                    ]
                    response = NetWorthList(net_worth_list=a)
                    return response
        except Exception as e:
            print(e)
            return {"message": "Could not get all assets"}
