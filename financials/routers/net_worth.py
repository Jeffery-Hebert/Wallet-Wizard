from fastapi import APIRouter, Depends, Response, status
from fastapi.security import HTTPBearer
from queries.net_worth_queries import NetWorth
from models.net_worth_models import NetWorthList
from utils import VerifyToken

router = APIRouter()
token_auth_scheme = HTTPBearer()


@router.get("/api/networths", response_model=NetWorthList)
def get_all_net_worth(
    net_worth_queries: NetWorth = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = net_worth_queries.getAll(userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result
