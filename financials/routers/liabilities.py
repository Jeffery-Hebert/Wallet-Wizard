from fastapi import APIRouter, Depends, Response, status
from fastapi.security import HTTPBearer
from models.liabilities_models import (
    LiabilityIn,
    LiabilityOut,
    LiabilitiesList,
)
from queries.liability_queries import Liabilities
from queries.net_worth_queries import NetWorth
from utils import VerifyToken

router = APIRouter()
token_auth_scheme = HTTPBearer()


@router.post("/api/liabilities/new", response_model=LiabilityOut)
def create_liability(
    new_liability: LiabilityIn,
    liability_queries: Liabilities = Depends(),
    net_worth_queries: NetWorth = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = liability_queries.create(new_liability, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        net_worth_queries.create(userID)
        return result


@router.get("/api/liabilities", response_model=LiabilitiesList)
def get_all_liabilities(
    liability_queries: Liabilities = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = liability_queries.getlist(userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.get("/api/liability/{id}", response_model=LiabilityOut)
def get_liability_by_id(
    id: str,
    liability_queries: Liabilities = Depends(),
    token: str = Depends(token_auth_scheme),
):

    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = liability_queries.getindv(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.delete("/api/liability/delete/{id}")
def delete_liability_by_id(
    id: str,
    liability_queries: Liabilities = Depends(),
    token: str = Depends(token_auth_scheme),
    net_worth_queries: NetWorth = Depends(),
):

    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = liability_queries.delete(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        net_worth_queries.create(userID)
        return result


@router.put("/api/liability/{id}", response_model=LiabilityOut)
def update_liability_by_id(
    id: str,
    liability: LiabilityIn,
    liability_queries: Liabilities = Depends(),
    net_worth_queries: NetWorth = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = liability_queries.edit(id, liability, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        net_worth_queries.create(userID)
        return result
