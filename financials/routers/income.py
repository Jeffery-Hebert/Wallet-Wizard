# Routers income.py


from fastapi import APIRouter, Depends, Response, status
from fastapi.security import HTTPBearer
from models.income import IncomeIn, IncomeList, IncomeOut
from queries.income_queries import Income
from utils import VerifyToken

router = APIRouter()
token_auth_scheme = HTTPBearer()


@router.get("/api/income/total")
def get_income_total(
    income_queries: Income = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = income_queries.total(userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.post("/api/income/new", response_model=IncomeOut)
def create_income(
    new_income: IncomeIn,
    income_queries: Income = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = income_queries.create(new_income, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.get("/api/income", response_model=IncomeList)
def get_all_income(
    income_queries: Income = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = income_queries.getlist(userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.get("/api/income/{id}", response_model=IncomeOut)
def get_income_by_id(
    id: str,
    income_queries: Income = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = income_queries.getindv(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.delete("/api/income/delete/{id}")
def delete_income_by_id(
    id: str,
    income_queries: Income = Depends(),
    token: str = Depends(token_auth_scheme),
):
    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = income_queries.delete(id, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result


@router.put("/api/income/{id}")
def update_income_by_id(
    id: str,
    income: IncomeIn,
    income_queries: Income = Depends(),
    token: str = Depends(token_auth_scheme),
):

    auth = VerifyToken(token.credentials).verify()
    userID = auth["sub"]

    result = income_queries.edit(id, income, userID)

    if auth.get("status"):
        Response.status_code = status.HTTP_400_BAD_REQUEST
        return auth
    else:
        return result
