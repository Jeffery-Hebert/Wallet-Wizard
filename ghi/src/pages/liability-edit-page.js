import EditLiabilityFormWithAuth from "../components/edit-liabilities-form"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const EditLiabilitiesPage = () => (
  <PageLayout>
    <EditLiabilityFormWithAuth />
  </PageLayout>
)
