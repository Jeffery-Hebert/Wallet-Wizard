import CreateIncomeFormWithAuth from "../components/create-income-form"
import { IncomeList } from "../components/list-income"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const IncomePage = () => (
  <PageLayout>
    <IncomeList />
    <CreateIncomeFormWithAuth />
  </PageLayout>
)
