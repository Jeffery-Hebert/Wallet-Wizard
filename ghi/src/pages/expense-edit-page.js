import EditExpenseFormWithAuth from "../components/edit-expenses-form"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const EditExpensePage = () => (
  <PageLayout>
    <EditExpenseFormWithAuth />
  </PageLayout>
)
