import EditIncomeFormWithAuth from "../components/edit-income-form"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const EditIncomePage = () => (
  <PageLayout>
    <EditIncomeFormWithAuth />
  </PageLayout>
)
