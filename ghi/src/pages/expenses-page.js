import CreateExpensesFormWithAuth from "../components/create-expenses-form"
import { ExpenseList } from "../components/list-expenses"
import { PageLayout } from "../components/page-layout"
import React from "react"

export const ExpensesPage = () => (
  <PageLayout>
    <ExpenseList />
    <CreateExpensesFormWithAuth />
  </PageLayout>
)
