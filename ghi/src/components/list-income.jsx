import { useEffect, useState } from "react";

import api_url from "../api_url";
import { useAuth0 } from "@auth0/auth0-react";
import { useNavigate } from "react-router-dom";

export const IncomeList = () => {
  const [income, setIncome] = useState([]);
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const { getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    const fetchIncome = async () => {
      try {
        const accessToken = await getAccessTokenSilently();
        const incomeUrl = `${api_url}income`;
        const fetchConfig = {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        };
        const incomeResponse = await fetch(incomeUrl, fetchConfig);
        let incomeData = await incomeResponse.json();
        setIncome(incomeData.income_list);
      } catch (error) {
        console.error(error);
        setError(true);
      }
    };
    fetchIncome();
  }, [getAccessTokenSilently]);

  async function deleteIncome(id) {
    try {
      const accessToken = await getAccessTokenSilently();
      const deleteUrl = `${api_url}income/delete/${id}`;
      const fetchConfig = {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
      };

      const delResponse = await fetch(deleteUrl, fetchConfig);
      if (delResponse.ok) {
        window.location.reload();
      } else {
        console.error("Error deleting post");
      }
    } catch (e) {
      console.error(e);
    }
  }

  if (error) {
    return <h1>Error fetching income</h1>;
  }
  if (income.length > 0) {
    return (
      <>
        <div className="container mt-4">
          <div className="row">
            <div className="col-md-12">
              <h1 className="mb-4 text-center">Income</h1>
              <table className="table table-striped table-hover">
                <thead className="thead-dark">
                  <tr>
                    <th>Income Name</th>
                    <th>Income Value</th>
                    <th>Income Type</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {income.map((income) => {
                    return (
                      <tr key={income.id}>
                        <td>{income.name}</td>
                        <td>${income.annualized_pay.toLocaleString()}</td>
                        <td>{income.pay_frequency}</td>
                        <td>
                          <button
                            className="btn btn-secondary"
                            onClick={() => navigate(`${income.id}`)}
                          >
                            {" "}
                            Edit{" "}
                          </button>
                        </td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => deleteIncome(income.id)}
                          >
                            {" "}
                            X
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
};
