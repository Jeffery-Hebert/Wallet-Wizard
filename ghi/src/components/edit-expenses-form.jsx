import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react"
import { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"

import api_url from "../api_url"

const types = [
  { label: "Car", value: "Car" },
  { label: "Housing", value: "Housing" },
  { label: "Utilities", value: "Utilities" },
  { label: "Food", value: "Food" },
  { label: "Phone/TV", value: "Phone/TV" },
  { label: "Entertainment/Fun", value: "Entertainment/Fun" },
  { label: "Other", value: "Other" },
]

export const EditExpenseForm = () => {
  const { id } = useParams()
  const [name, setName] = useState("")
  const [amount, setAmount] = useState("")
  const [type, setType] = useState("Car")
  const navigate = useNavigate()

  const { getAccessTokenSilently } = useAuth0()

  useEffect(() => {
    async function fetchExpense() {
      try {
        const expenseUrl = `${api_url}expense/${id}`
        const accessToken = await getAccessTokenSilently()
        const expenseResponse = await fetch(expenseUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        })
        let expenseData = await expenseResponse.json()
        setAmount(expenseData.amount)
        setType(expenseData.type)
        setName(expenseData.name)
      } catch (error) {
        console.error(error)
      }
    }
    fetchExpense()
  }, [getAccessTokenSilently, id])

  async function editExpense(data) {
    const url = `${api_url}expense/${id}`

    const accessToken = await getAccessTokenSilently()

    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    }
    await fetch(url, fetchConfig)
    navigate(`/expenses`)
  }

  function handleSubmit(e) {
    e.preventDefault()
    editExpense({
      name: name,
      amount: amount,
      type: type,
    })
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Edit an Exsisting Expense</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              className="form-control"
              id="Value"
              rows="3"
              placeholder="Value"
              required
            ></input>
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">Expense Category</label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option
                value=""
                disabled
              >
                Expense Category
              </option>
              {types.map((frequency) => (
                <option
                  key={frequency.value}
                  value={frequency.value}
                >
                  {frequency.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  )
}

const EditExpenseFormWithAuth = withAuthenticationRequired(EditExpenseForm)

export default EditExpenseFormWithAuth
