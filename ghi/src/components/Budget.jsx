import { useEffect, useState } from "react";
import api_url from "../api_url";
import { useAuth0 } from "@auth0/auth0-react";

export const Budget = () => {
  const [income, setIncome] = useState([]);
  const [expenses, setExpenses] = useState([]);
  const [error, setError] = useState(false);
  const { getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    async function fetchIncomeandExpenses() {
      try {
        const accessToken = await getAccessTokenSilently();

        const expensesUrl = `${api_url}expenses/categories`;
        const incomeUrl = `${api_url}income/total`;

        const fetchConfig = {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        };

        const expensesResponse = await fetch(expensesUrl, fetchConfig);
        const incomeResponse = await fetch(incomeUrl, fetchConfig);

        const expensesData = await expensesResponse.json();
        const incomeData = await incomeResponse.json();

        setIncome(incomeData);
        setExpenses(expensesData.types);
      } catch (e) {
        console.error(e);
        setError(true);
      }
    }
    fetchIncomeandExpenses();
  }, [getAccessTokenSilently]);

  const categories = {
    Car: 15,
    Utilities: 5,
    Housing: 30,
    Food: 10,
    "Entertainment/Fun": 10,
    "Phone/TV": 5,
    Other: 5,
  };

  if (error) {
    return <h1>Error fetching expenses or income</h1>;
  }
  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-12">
          <h1 className="mb-4 text-center">Budget</h1>
          <div className="table-responsive">
            <table className="table table-striped table-hover">
              <thead className="thead-dark">
                <tr>
                  <th>Expense Category</th>
                  <th>Percent of income</th>
                  <th>Notes</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {expenses.map(({ type, total }) => {
                  const percent = Math.round((total / (income / 12)) * 100);
                  const limit = categories[type];
                  const notes =
                    percent > limit
                      ? `The suggested spending limit for ${type} is ${limit}%. Consider cutting ${type} expenses.`
                      : "";

                  return (
                    <tr key={type}>
                      <td>{type}</td>
                      <td>{percent}%</td>
                      <td>{notes}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};
