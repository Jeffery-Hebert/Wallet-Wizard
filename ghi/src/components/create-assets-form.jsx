import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";

import api_url from "../api_url";
import { useState } from "react";

const types = [
  { label: "Liquid", value: "liquid" },
  { label: "Semi-Liquid", value: "semi-liquid" },
  { label: "Illiquid", value: "illiquid" },
];

const CreateAssetForm = () => {
  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const [type, setType] = useState("liquid");

  const { getAccessTokenSilently } = useAuth0();

  async function createAsset(data) {
    const url = `${api_url}assets/new`;

    const accessToken = await getAccessTokenSilently();

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    };

    try {
      await fetch(url, fetchConfig);
      window.location.reload();
    } catch (error) {
      console.error("Failed to create asset:", error);
    }
  }

  async function handleSubmit(e) {
    e.preventDefault();

    createAsset({
      name: name,
      value: value,
      type: type,
    });
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Create a new Asset!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              className="form-control"
              id="Value"
              rows="3"
              placeholder="Value"
              required
            />
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">Choose a type:</label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option value="" disabled>
                Select a Type
              </option>
              {types.map((type) => (
                <option key={type.value} value={type.value}>
                  {type.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  );
};

const CreateAssetFormWithAuth = withAuthenticationRequired(CreateAssetForm);

export default CreateAssetFormWithAuth;
