import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react"
import { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"

import api_url from "../api_url"

const pay_frequencies = [
  { label: "Weekly", value: "Weekly" },
  { label: "Bi-Weekly", value: "Bi-Weekly" },
  { label: "Twice a month", value: "Twice a month" },
  { label: "Monthly", value: "Monthly" },
  { label: "Other", value: "Other" },
]

export const EditIncomeForm = () => {
  const { id } = useParams()
  const [name, setName] = useState("")
  const [annualized_pay, setAnnualizedPay] = useState("")
  const [pay_frequency, setPayFrequency] = useState("Weekly")
  const navigate = useNavigate()

  const { getAccessTokenSilently } = useAuth0()

  useEffect(() => {
    async function fetchIncome() {
      try {
        const incomeUrl = `${api_url}income/${id}`
        const accessToken = await getAccessTokenSilently()
        const incomeResponse = await fetch(incomeUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        })
        let incomeData = await incomeResponse.json()
        setAnnualizedPay(incomeData.annualized_pay)
        setPayFrequency(incomeData.pay_frequency)
        setName(incomeData.name)
      } catch (error) {
        console.error(error)
      }
    }
    fetchIncome()
  }, [getAccessTokenSilently, id])

  async function editIncome(data) {
    const url = `${api_url}income/${id}`

    const accessToken = await getAccessTokenSilently()

    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    }
    await fetch(url, fetchConfig)
    navigate(`/income`)
  }

  async function handleSubmit(e) {
    e.preventDefault()
    editIncome({
      name: name,
      annualized_pay: annualized_pay,
      pay_frequency: pay_frequency,
    })
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Edit an Exsisting Income</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={annualized_pay}
              onChange={(e) => setAnnualizedPay(e.target.value)}
              className="form-control"
              id="Value"
              rows="3"
              placeholder="Value"
              required
            ></input>
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">How often are you paid?</label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setPayFrequency(e.target.value)}
              required
            >
              <option
                value=""
                disabled
              >
                Select a Pay Frequency
              </option>
              {pay_frequencies.map((frequency) => (
                <option
                  key={frequency.value}
                  value={frequency.value}
                >
                  {frequency.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  )
}

const EditIncomeFormWithAuth = withAuthenticationRequired(EditIncomeForm)

export default EditIncomeFormWithAuth
