import { NavBarTab } from "./nav-bar-tab";
import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

export const NavBarTabs = () => {
  const { isAuthenticated } = useAuth0();

  return (
    <div className="nav-bar__tabs">
      <NavBarTab path="/" label="Home" />
      {isAuthenticated && (
        <>
          <NavBarTab path="/networth" label="Net Worth" />
          <NavBarTab path="/budget" label="Budget" />
          <NavBarTab path="/assets" label="Assets" />
          <NavBarTab path="/liabilities" label="Liabilities" />
          <NavBarTab path="/income" label="Income" />
          <NavBarTab path="/expenses" label="Expenses" />
          <NavBarTab path="/advisor" label="Ask an AI Advisor" />
        </>
      )}
    </div>
  );
};
