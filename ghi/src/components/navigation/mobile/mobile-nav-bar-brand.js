import React from "react";
import { NavLink } from "react-router-dom";

import logo from "./Logo.png";

// TODO: Update Mobile Nav Bar

export const MobileNavBarBrand = ({ handleClick }) => {
  return (
    <div onClick={handleClick} className="mobile-nav-bar__brand">
      <NavLink to="/">
        <img className="nav-bar__logo" src={logo} alt="Wallet Wizard logo" />
      </NavLink>
    </div>
  );
};
