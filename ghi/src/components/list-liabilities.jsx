import { useEffect, useState } from "react";

import api_url from "../api_url";
import { useAuth0 } from "@auth0/auth0-react";
import { useNavigate } from "react-router-dom";

export const LiabilityList = () => {
  const [liabilities, setLiabilities] = useState([]);
  const [error, setError] = useState(false);
  const navigate = useNavigate();
  const { getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    const fetchLiability = async () => {
      try {
        const accessToken = await getAccessTokenSilently();
        const liabilityUrl = `${api_url}liabilities`;
        const fetchConfig = {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        };
        const liabilityResponse = await fetch(liabilityUrl, fetchConfig);
        const liabilityData = await liabilityResponse.json();
        setLiabilities(liabilityData.liabilities);
      } catch (e) {
        console.error(e);
        setError(true);
      }
    };

    fetchLiability();
  }, [getAccessTokenSilently]);

  async function deleteLiability(id) {
    try {
      const accessToken = await getAccessTokenSilently();
      const deleteUrl = `${api_url}liability/delete/${id}`;
      const fetchConfig = {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
      };

      const delResponse = await fetch(deleteUrl, fetchConfig);
      if (delResponse.ok) {
        window.location.reload();
      } else {
        console.error("Error deleting post");
      }
    } catch (e) {
      console.error(e);
    }
  }

  if (error) {
    return <h1>Error fetching liabilities</h1>;
  }
  if (liabilities.length > 0) {
    return (
      <>
        <div className="container mt-4">
          <div className="row">
            <div className="col-md-12">
              <h1 className="mb-4 text-center">Liabilities</h1>
              <table className="table table-striped table-hover">
                <thead className="thead-dark">
                  <tr>
                    <th>Liability Name</th>
                    <th>Liability Value</th>
                    <th>Liability Type</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {liabilities.map((liability) => {
                    return (
                      <tr key={liability.id}>
                        <td>{liability.name}</td>
                        <td>${liability.value.toLocaleString()}</td>
                        <td>{liability.type}</td>
                        <td>
                          <button
                            className="btn btn-secondary"
                            onClick={() => navigate(`${liability.id}`)}
                          >
                            {" "}
                            Edit{" "}
                          </button>
                        </td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => deleteLiability(liability.id)}
                          >
                            {" "}
                            X
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
};
