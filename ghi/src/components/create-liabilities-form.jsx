import { useAuth0, withAuthenticationRequired } from "@auth0/auth0-react";

import api_url from "../api_url";
import { useState } from "react";

const types = [
  { label: "90 days or less", value: "short-term" },
  { label: "90-365 days", value: "intermediate-term" },
  { label: "365 days +", value: "long-term" },
];

const CreateLiabilityForm = () => {
  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const [type, setType] = useState("short-term");

  const { getAccessTokenSilently } = useAuth0();

  async function createLiability(data) {
    const url = `${api_url}liabilities/new`;

    const accessToken = await getAccessTokenSilently();

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    };

    try {
      await fetch(url, fetchConfig);
      window.location.reload();
    } catch (error) {
      console.error("Failed to create asset:", error);
    }
  }

  async function handleSubmit(e) {
    e.preventDefault();

    createLiability({
      name: name,
      value: value,
      type: type,
    });
  }

  return (
    <div className="container">
      <div className="shadow p-4 mt-4 rounded">
        <h1>Create a new Liability!</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name"
              required
              type="text"
              name="Name"
              id="Name"
              className="form-control"
            />
            <label htmlFor="Name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              className="form-control"
              id="Value"
              rows="3"
              placeholder="Value"
              required
            ></input>
          </div>
          <div className="form-group mb-3">
            <label htmlFor="Type">
              When do you need to have the full balance paid off by?
            </label>
            <select
              className="form-select"
              id="Type"
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option value="" disabled>
                Select a Type
              </option>
              {types.map((type) => (
                <option key={type.value} value={type.value}>
                  {type.label}
                </option>
              ))}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  );
};

const CreateLiabilityFormWithAuth =
  withAuthenticationRequired(CreateLiabilityForm);

export default CreateLiabilityFormWithAuth;
