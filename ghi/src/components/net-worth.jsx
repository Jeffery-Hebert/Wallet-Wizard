import {
  CartesianGrid,
  Label,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { useEffect, useState } from "react";

import api_url from "../api_url";
import { useAuth0 } from "@auth0/auth0-react";

export const NetWorth = () => {
  const [netWorthData, setNetWorthData] = useState([]);
  const [error, setError] = useState(false);
  const { getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    const fetchNetWorth = async () => {
      try {
        const accessToken = await getAccessTokenSilently();
        const allNetWorthUrl = `${api_url}networths`;
        const fetchConfig = {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        };
        const allNetWorthResponse = await fetch(allNetWorthUrl, fetchConfig);
        const allNetWorthData = await allNetWorthResponse.json();

        const uniqueDates = [
          ...new Set(
            allNetWorthData.net_worth_list.map((entry) =>
              entry.date.substring(0, 10)
            )
          ),
        ];
        const lastNetWorthEntries = uniqueDates.map((date) =>
          allNetWorthData.net_worth_list
            .filter((entry) => entry.date.substring(0, 10) === date)
            .shift()
        );

        setNetWorthData(lastNetWorthEntries);
      } catch (e) {
        console.error(e);
        setError(true);
      }
    };
    fetchNetWorth();
  }, [getAccessTokenSilently]);

  if (error) {
    return <h1>Error fetching net worth</h1>;
  }

  if (netWorthData.length > 0) {
    return (
      <div className="d-flex justify-content-center">
        <div className="shadow-none p-3 mb-5 post-color rounded p-4 mt-4">
          <h1 className="chart-title">Net Worth Over Time</h1>
          <h2 className="current-net-worth">
            Your current net worth is ${netWorthData[0].value.toLocaleString()}
          </h2>
          <LineChart
            width={800}
            height={400}
            data={netWorthData}
            margin={{ top: 20, right: 30, bottom: 20, left: 30 }}
          >
            <Line
              type="monotone"
              dataKey="value"
              stroke="#4c96d7"
              strokeWidth={3}
              dot={false}
            />
            <CartesianGrid stroke="#e0e0e0" vertical={false} />
            <XAxis
              dataKey="date"
              tickFormatter={(tick) => new Date(tick).toLocaleDateString()}
              axisLine={false}
              tickLine={false}
              reversed={true}
            >
              <Label
                value="Date"
                position="insideBottom"
                offset={-5}
                fill="#6b6b6b"
              />
            </XAxis>
            <YAxis
              dataKey="value"
              axisLine={false}
              tickLine={false}
              tickFormatter={(tick) => `$${tick.toLocaleString()}`}
            >
              <Label
                value="Net Worth"
                position="insideLeft"
                angle={-90}
                offset={-10}
                fill="#6b6b6b"
              />
            </YAxis>
            <Tooltip
              formatter={(value) => `$${value.toLocaleString()}`}
              labelFormatter={(label) => new Date(label).toLocaleDateString()}
            />
          </LineChart>
        </div>
      </div>
    );
  }
};
